package csu.coderwu.chelper.redis;

import csu.coderwu.chelper.model.entity.Login;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author coderWu
 * @date 17-12-21 下午4:59
 */
@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Login> loginRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Login> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new RedisObjectSerializer());
        return template;
    }


}
