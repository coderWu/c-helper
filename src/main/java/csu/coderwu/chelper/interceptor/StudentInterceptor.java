package csu.coderwu.chelper.interceptor;

import csu.coderwu.chelper.service.LoginService;
import csu.coderwu.chelper.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 检测教务系统账户密码时效性
 * Created by coderWu on 17-6-11.
 */
public class StudentInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private StudentService studentService;

    @Autowired
    private LoginService loginService;

    @Override
    public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response,
            Object handler) throws Exception {
//        String session = request.getHeader("session");
//        Login login = loginService.selectBySession(session);
//        Student student = studentService.getStudentByOpenId(login.getOpenid());
//        if (student != null){
//            String cookie = studentService.login(student.getSchoolNumber());
//            if (studentService.checkPwd(student, cookie) == null) {
//                studentService.deleteStudent(login.getOpenid());
//                response.sendRedirect(request.getContextPath() + "/student/notbind");
//                return false;
//            }
//        } else {
//            response.sendRedirect(request.getContextPath() + "/student/notbind");
//            return false;
//        }
        return true;
    }

}
