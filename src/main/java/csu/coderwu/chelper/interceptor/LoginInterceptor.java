package csu.coderwu.chelper.interceptor;

import csu.coderwu.chelper.config.Global;
import csu.coderwu.chelper.model.entity.Login;
import csu.coderwu.chelper.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 登录拦截
 * @author coderwu
 * Created by coderWu on 17-6-11.
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private LoginService loginService;

    @Override
    public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response,
            Object handler) throws Exception {
        String session = request.getHeader("session");
        Login login = loginService.selectBySession(session);
        boolean uselessSession =
                Long.valueOf(login.getLoginTime()) + Global.SESSION_TIME_LIMIT
                        < System.currentTimeMillis();
        if ( uselessSession || session == null || "".equals(session) || loginService.selectBySession(session) == null) {
            response.sendRedirect(request.getContextPath() + "/login/notlogin");
            return false;
        }
        return true;
    }

}
