package csu.coderwu.chelper.controller;

import csu.coderwu.chelper.model.entity.Cet;
import csu.coderwu.chelper.model.entity.Student;
import csu.coderwu.chelper.service.CsuCetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * CET score
 * Created by coderWu on 17-6-12.
 */
@RestController
@RequestMapping("/cet")
public class CetController {

    @Autowired
    private CsuCetService csuCetService;

    @RequestMapping(value = "/getgrade", method = RequestMethod.POST)
    public List<Cet> getGrade(@RequestBody Student student) {
        return csuCetService.getCetGrades(student);
    }

}
