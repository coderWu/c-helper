package csu.coderwu.chelper.controller;

import csu.coderwu.chelper.config.ErrorCode;
import csu.coderwu.chelper.model.dto.ErrorResponse;
import csu.coderwu.chelper.model.entity.Course;
import csu.coderwu.chelper.model.entity.Login;
import csu.coderwu.chelper.model.entity.Student;
import csu.coderwu.chelper.service.LoginService;
import csu.coderwu.chelper.service.StudentService;
import csu.coderwu.chelper.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Student 相关控制器
 * @author coderWu
 * Created by coderWu on 17-6-10.
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    private static final String[] GRADE_YEARS = {
            "2014-2015-1", "2014-2015-2", "2015-2016-1", "2015-2016-2",
            "2016-2017-1", "2016-2017-2", "2017-2018-1"
    };

    @Autowired
    private StudentService studentService;

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/bind", method = RequestMethod.POST)
    public Object bind(@RequestBody Student student, HttpServletRequest request) {
        String session = request.getHeader("session");
        Login login = loginService.selectBySession(session);
        if (!studentService.login(student.getSchoolNumber(), student.getEduSystemPwd())) {
            return new ErrorResponse(ErrorCode.INVALID_PARAM, "password or school number wrong");
        }
        student = studentService.getStudentInfo(student);
        student.setOpenId(login.getOpenid());
        studentService.bind(student);
        HashMap<String, Student> result = new HashMap<>(1);
        result.put("student", Return.returnStudent(student));
        return result;
    }

    @RequestMapping(value = "/cancle", method = RequestMethod.POST)
    public Map<String, Object> cancle(HttpServletRequest request) {
        String session = request.getHeader("session");
        Login login = loginService.selectBySession(session);
        HashMap<String, Object> result = new HashMap<>(1);
        result.put("cancle", studentService.deleteStudent(login.getOpenid()));
        return result;
    }

    @RequestMapping(value = "/getgrade", method = RequestMethod.POST)
    public List<Course> getGrade(@RequestBody String year, HttpServletRequest request) {
        boolean is = false;
        for (String item : GRADE_YEARS) {
            if (item.equals(year)) {
                is = true;
                break;
            }
        }
        if (!is) {
            return null;
        }
        String session = request.getHeader("session");
        Login login = loginService.selectBySession(session);
        Student student = studentService.getStudentByOpenId(login.getOpenid());
        return studentService.getGrade(student, year);
    }

    @RequestMapping(value = "/notbind", method = RequestMethod.GET)
    public ErrorResponse notBind() {
        return new ErrorResponse(ErrorCode.FAILED, "not bind or your password has changed");
    }

}
