package csu.coderwu.chelper.controller;

import csu.coderwu.chelper.config.ErrorCode;
import csu.coderwu.chelper.model.dto.ErrorResponse;
import csu.coderwu.chelper.model.entity.Student;
import csu.coderwu.chelper.model.entity.User;
import csu.coderwu.chelper.service.LoginService;
import csu.coderwu.chelper.service.StudentService;
import csu.coderwu.chelper.service.UserService;
import csu.coderwu.chelper.util.MD5Encode;
import csu.coderwu.chelper.util.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;


/**
 * User 相关控制器
 * @author coderWu
 * Created by coderWu on 17-6-10.
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Object login(@RequestBody User user) {
        String code = user.getCode();
        //用户openid
        String openid = userService.login(code);
        if (openid == null) {
            return new ErrorResponse()
                    .setErrcode(ErrorCode.INVALID_PARAM)
                    .setErrmsg("invalid code");
        }
        //更新用户信息
        user.setOpenId(openid);
        if (userService.hasUser(openid)) {
            userService.updateUser(user);
        } else {
            userService.addUser(user);
        }

        //登陆的时候检测绑定的账号密码是否有效，无效删除绑定
        Student student = studentService.getStudentByOpenId(openid);
        if (student != null) {
            if (!studentService.login(student.getSchoolNumber(), student.getEduSystemPwd())) {
                studentService.deleteStudent(openid);
                student = null;
            }
        }

        //保存第三方session登录到数据库
        String session = MD5Encode.StrMD5(code);
        loginService.add(session, openid);

        HashMap<String, Object> result = new HashMap<>(3);
        result.put("session", session);
        result.put("time", System.currentTimeMillis());
        result.put("student", Return.returnStudent(student));
        return result;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(HttpServletRequest request) {
        String session = request.getHeader("session");
        loginService.delete(session);
        return "";
    }

    @RequestMapping(value = "/notlogin", method = RequestMethod.GET)
    public ErrorResponse notLogin() {
        return new ErrorResponse()
                .setErrcode(ErrorCode.NOT_LOGIN)
                .setErrmsg("not login");
    }

}
