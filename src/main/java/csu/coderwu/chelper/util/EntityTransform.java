package csu.coderwu.chelper.util;

import csu.coderwu.chelper.model.entity.Cet;
import csu.coderwu.chelper.model.entity.Course;
import csu.coderwu.chelper.model.entity.Student;
import csu.coderwu.csuhelper.cet.bean.CetScore;
import csu.coderwu.csuhelper.es.bean.Score;
import sun.reflect.generics.scope.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author coderWu
 * @date 17-12-21 下午5:19
 */
public class EntityTransform {

    public static Cet fromCetScoreToCet(CetScore cetScore) {
        return cetScore == null ? null : new Cet().setCertificateID(cetScore.getCertificateId())
                .setExamNumber(cetScore.getExamNumber())
                .setLevel(cetScore.getLevel())
                .setScore(String.valueOf(cetScore.getScore()))
                .setTime(cetScore.getDate());
    }

    public static List<Cet> fromCetScoreToCet(List<CetScore> cetScores) {

        if (cetScores == null) {
            return null;
        }

        int count = cetScores.size();
        List<Cet> cets = new ArrayList<>(count);

        for (CetScore cetScore : cetScores) {
            cets.add(fromCetScoreToCet(cetScore));
        }
        return cets;
    }

    public static Student fromEsStudentToStudent(csu.coderwu.csuhelper.es.bean.Student student, Student student2) {
        return student == null ? null : new Student().setGrade(student.getGrade())
                .setMajor(student.getClassName())
                .setSchoolNumber(student.getXh())
                .setSex(student.getGender())
                .setUsername(student.getName())
                .setEduSystemPwd(student2.getEduSystemPwd());
    }

    public static Course fromScoreToCourse(Score score) {
        return score == null ? null : new Course().setName(score.getCourseName())
                .setScore(score.getScore());
    }

    public static List<Course> fromScoreToCourse(List<Score> scores) {

        if (scores == null) {
            return null;
        }

        int count = scores.size();
        List<Course> courses = new ArrayList<>(count);
        for (Score score : scores) {
            courses.add(fromScoreToCourse(score));
        }
        return courses;
    }

}
