package csu.coderwu.chelper.util;

import sun.misc.BASE64Encoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5 加密
 * Created by coderWu on 17-6-10.
 */
public class MD5Encode {

    public static String StrMD5(String message) {
        try {
            MessageDigest md;
            md = MessageDigest.getInstance("md5");
            byte m5[] = md.digest(message.getBytes());
            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(m5);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;

    }

}
