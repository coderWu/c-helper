package csu.coderwu.chelper.util;

import csu.coderwu.chelper.model.entity.Student;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回controller结果
 * Created by coderWu on 17-6-11.
 */
public class Return {

    public static Student returnStudent(Student student) {
        if (student == null) {
            return null;
        }
        student.setOpenId("");
        student.setEduSystemPwd("");
        return student;
    }
}
