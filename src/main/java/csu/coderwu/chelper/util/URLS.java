package csu.coderwu.chelper.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 * 模拟登陆获取页面内容
 * Created by coderWu on 17-6-11.
 */
public class URLS {

    public static String getContentWithToken(String token, String url) {
        String pageContent;
        try {
            URL realUrl = new URL(url);
            URLConnection conn = realUrl.openConnection();
            conn.setRequestProperty("token", token);
            conn.connect();
            pageContent = readFromUrlConnection(conn);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return pageContent;
    }

    public static JSONObject readJsonFromUrl(String url, String token) {
        try {
            URL realUrl = new URL(url);
            URLConnection conn = realUrl.openConnection();
            conn.setRequestProperty("token", token);
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            JSONParser parser = new JSONParser();
            JSONObject result = (JSONObject) parser.parse(rd);
            is.close();
            rd.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private static String readFromUrlConnection(URLConnection connection) {
        StringBuilder pageContent = new StringBuilder();
        try (
                InputStream is = connection.getInputStream()
        ){
            int hasRead;
            byte[] bbuf = new byte[1024];
            while ((hasRead = is.read(bbuf)) >= 0) {
                pageContent.append(new String(bbuf, 0, hasRead));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return pageContent.toString();
    }

}

