package csu.coderwu.chelper.model.dto;

/**
 * @author coderWu
 * @date 17-12-21 下午4:06
 */
public class ErrorResponse {

    private String errcode;
    private String errmsg;

    public ErrorResponse() {}

    public ErrorResponse(String errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public String getErrcode() {
        return errcode;
    }

    public ErrorResponse setErrcode(String errcode) {
        this.errcode = errcode;
        return this;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public ErrorResponse setErrmsg(String errmsg) {
        this.errmsg = errmsg;
        return this;
    }
}
