package csu.coderwu.chelper.model.entity;

/**
 * 课程
 * Created by coderWu on 17-6-11.
 */
public class Course {

    private String name;

    private String score;

    public String getName() {
        return name;
    }

    public Course setName(String name) {
        this.name = name;
        return this;
    }

    public String getScore() {
        return score;
    }

    public Course setScore(String score) {
        this.score = score;
        return this;
    }
}
