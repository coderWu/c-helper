package csu.coderwu.chelper.model.entity;

/**
 * CET
 * Created by coderWu on 17-6-12.
 */
public class Cet {

    private String score;

    private String examNumber;

    private String certificateID;

    private String level;

    private String time;

    public String getScore() {
        return score;
    }

    public Cet setScore(String score) {
        this.score = score;
        return this;
    }

    public String getExamNumber() {
        return examNumber;
    }

    public Cet setExamNumber(String examNumber) {
        this.examNumber = examNumber;
        return this;
    }

    public String getCertificateID() {
        return certificateID;
    }

    public Cet setCertificateID(String certificateID) {
        this.certificateID = certificateID;
        return this;
    }

    public String getLevel() {
        return level;
    }

    public Cet setLevel(String level) {
        this.level = level;
        return this;
    }

    public String getTime() {
        return time;
    }

    public Cet setTime(String time) {
        this.time = time;
        return this;
    }
}
