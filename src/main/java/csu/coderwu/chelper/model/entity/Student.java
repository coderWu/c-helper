package csu.coderwu.chelper.model.entity;

/**
 * Student POJO
 * Created by coderWu on 17-6-9.
 */
public class Student {

    private String openId;

    private String schoolNumber;

    private String username;

    private String phone;

    private String grade;

    private String major;

    private String sex;

    private String eduSystemPwd;

    public String getOpenId() {
        return openId;
    }

    public Student setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public Student setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Student setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Student setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public Student setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public String getMajor() {
        return major;
    }

    public Student setMajor(String major) {
        this.major = major;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public Student setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getEduSystemPwd() {
        return eduSystemPwd;
    }

    public Student setEduSystemPwd(String eduSystemPwd) {
        this.eduSystemPwd = eduSystemPwd;
        return this;
    }
}
