package csu.coderwu.chelper.model.entity;

import java.io.Serializable;

/**
 * 登录
 * Created by coderWu on 17-6-11.
 */
public class Login implements Serializable {

    private String session;

    private String openid;

    private String loginTime;

    public String getSession() {
        return session;
    }

    public Login setSession(String session) {
        this.session = session;
        return this;
    }

    public String getOpenid() {
        return openid;
    }

    public Login setOpenid(String openid) {
        this.openid = openid;
        return this;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public Login setLoginTime(String loginTime) {
        this.loginTime = loginTime;
        return this;
    }
}
