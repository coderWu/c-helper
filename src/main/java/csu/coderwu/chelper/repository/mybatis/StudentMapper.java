package csu.coderwu.chelper.repository.mybatis;


import csu.coderwu.chelper.repository.StudentRepository;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by coderWu on 17-6-9.
 */
@Mapper
public interface StudentMapper extends StudentRepository {

}
