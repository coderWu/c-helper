package csu.coderwu.chelper.repository.mybatis;

import csu.coderwu.chelper.repository.UserRepository;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by coderWu on 17-6-8.
 */
@Mapper
public interface UserMapper extends UserRepository {

}
