package csu.coderwu.chelper.repository;

import csu.coderwu.chelper.model.entity.Student;

/**
 * Student Dao
 * Created by coderWu on 17-6-9.
 */
public interface StudentRepository {

    int addStudent(Student student);

    Student getStudentByOpenId(String openId);

    int update(Student student);

    int delete(String openid);

}
