package csu.coderwu.chelper.repository;

import csu.coderwu.chelper.model.entity.User;

/**
 * User Dao
 * Created by coderWu on 17-6-8.
 */
public interface UserRepository {

    int addUser(User user);

    int updateUser(User user);

    User getUserByOpenId(String openId);
}
