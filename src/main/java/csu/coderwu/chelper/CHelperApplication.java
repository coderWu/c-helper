package csu.coderwu.chelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CHelperApplication {

	public static void main(String[] args) {
		SpringApplication.run(CHelperApplication.class, args);
	}
}
