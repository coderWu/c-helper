package csu.coderwu.chelper.service;

import csu.coderwu.chelper.model.entity.Cet;
import csu.coderwu.chelper.model.entity.Student;

import java.util.List;

/**
 * CET Service
 * Created by coderWu on 17-6-12.
 */
public interface CsuCetService {

    List<Cet> getCetGrades(Student student);

}
