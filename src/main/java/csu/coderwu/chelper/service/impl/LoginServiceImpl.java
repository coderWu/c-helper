package csu.coderwu.chelper.service.impl;

import csu.coderwu.chelper.model.entity.Login;
import csu.coderwu.chelper.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 登录态接口实现
 * @author coderwu
 * Created by coderWu on 17-6-11.
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    private RedisTemplate<String, Login> loginRedisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean add(String session, String openid) {
        if (session != null && openid != null
                && !"".equals(session) && !"".equals(openid)) {
            Login login = this.loginRedisTemplate.opsForValue().get(openid);
            if (login == null) {
                login = new Login();
                login.setOpenid(openid);
            }
            login.setSession(session);
            login.setLoginTime(String.valueOf(System.currentTimeMillis()));
            this.update(login);
            return true;
        }
        return false;
    }

    @Override
    public Login selectByOpenid(String openid) {
        return loginRedisTemplate.opsForValue().get(openid);
    }

    @Override
    public Login selectBySession(String session) {
        String openid = stringRedisTemplate.opsForValue().get(session);
        return loginRedisTemplate.opsForValue().get(openid == null ? "" : openid);
    }

    @Override
    public void delete(String session) {
        stringRedisTemplate.delete(session);
    }

    @Override
    public void update(Login login) {
        loginRedisTemplate.opsForValue().set(login.getOpenid(), login);
        stringRedisTemplate.opsForValue().set(login.getSession(), login.getOpenid());
    }
}
