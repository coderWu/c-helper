package csu.coderwu.chelper.service.impl;

import csu.coderwu.chelper.model.entity.Cet;
import csu.coderwu.chelper.model.entity.Student;
import csu.coderwu.chelper.service.CsuCetService;
import csu.coderwu.chelper.util.EntityTransform;
import csu.coderwu.csuhelper.cet.api.CetService;
import csu.coderwu.csuhelper.cet.bean.CetScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * CET 功能实现
 * @author coderwu
 * Created by coderWu on 17-6-12.
 */
@Service("csuCetService")
public class CsuCetServiceImpl implements CsuCetService {

    @Autowired
    CetService cetService;


    @Override
    public List<Cet> getCetGrades(Student student) {
        if (student == null) {
            return null;
        }
        List<CetScore> cetScores = cetService.getHistoryScores(student.getSchoolNumber(), student.getUsername());
        return EntityTransform.fromCetScoreToCet(cetScores);
    }

}
