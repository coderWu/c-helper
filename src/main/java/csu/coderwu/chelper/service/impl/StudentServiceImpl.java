package csu.coderwu.chelper.service.impl;

import csu.coderwu.chelper.model.entity.Course;
import csu.coderwu.chelper.model.entity.Student;
import csu.coderwu.chelper.repository.mybatis.StudentMapper;
import csu.coderwu.chelper.service.StudentService;
import csu.coderwu.chelper.util.EntityTransform;
import csu.coderwu.chelper.util.URLS;
import csu.coderwu.csuhelper.es.api.EsService;
import csu.coderwu.csuhelper.es.bean.Score;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Student Service 实现
 * Created by coderWu on 17-6-10.
 */
@Service("studentService")
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private EsService esService;

    /**
     * 登录获取token
     * @param schoolNum string
     * @param pwd string
     * @return boolean
     */
    @Override
    public boolean login(String schoolNum,String pwd) {
        return esService.loginCheck(schoolNum, pwd);
    }

    @Override
    public Student getStudentInfo(Student student) {
        csu.coderwu.csuhelper.es.bean.Student esStudent =
                esService.getStudentInfo(student.getSchoolNumber(), student.getEduSystemPwd());
        return EntityTransform.fromEsStudentToStudent(esStudent, student);
    }

    /**
     * 获取成绩
     */
    @Override
    public List<Course> getGrade(Student student, String year) {
        List<Score> scores = esService.getScore(student.getSchoolNumber(), student.getEduSystemPwd(), year);
        return EntityTransform.fromScoreToCourse(scores);
    }

    @Override
    public int bind(Student student) {
        if (isBind(student.getOpenId())) {
            return studentMapper.update(student);
        } else {
            return studentMapper.addStudent(student);
        }
    }

    @Override
    public boolean isBind(String openId) {
        return null != studentMapper.getStudentByOpenId(openId);
    }

    @Override
    public boolean deleteStudent(String openId) {
        return studentMapper.delete(openId) > 0;
    }

    @Override
    public Student getStudentByOpenId(String openid) {
        if (openid == null || "".equals(openid) || !isBind(openid)) {
            return null;
        } else {
            return studentMapper.getStudentByOpenId(openid);
        }
    }

}
