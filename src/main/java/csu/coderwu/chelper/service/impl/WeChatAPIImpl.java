package csu.coderwu.chelper.service.impl;

import csu.coderwu.chelper.config.WxProperties;
import csu.coderwu.chelper.service.WeChatAPI;
import csu.coderwu.chelper.util.URLS;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 *WeChatAPI 接口实现
 * Created by coderWu on 17-6-9.
 */
@Service("weChat")
@EnableConfigurationProperties(WxProperties.class)
public class WeChatAPIImpl implements WeChatAPI {

    @Autowired
    private WxProperties wxProperties;

    private String getSeesionKeyAndOpenidUrl(String code) {
        return "https://api.weixin.qq.com/sns/jscode2session?appid=" +
                wxProperties.getAppid() + "&secret=" + wxProperties.getSecret() + "&js_code=" + code +
                "&grant_type=authorization_code";
    }

    @Override
    public JSONObject getSessionKeyAndOpenid(String code) {
        if (code == null || "".equals(code)) {
            return null;
        }
        String url = this.getSeesionKeyAndOpenidUrl(code);
        return URLS.readJsonFromUrl(url, "");
    }
}
