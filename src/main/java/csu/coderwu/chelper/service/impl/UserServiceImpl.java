package csu.coderwu.chelper.service.impl;

import csu.coderwu.chelper.model.entity.User;
import csu.coderwu.chelper.repository.mybatis.UserMapper;
import csu.coderwu.chelper.service.UserService;
import csu.coderwu.chelper.service.WeChatAPI;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *User Service 接口实现
 * Created by coderWu on 17-6-10.
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private WeChatAPI weChatAPI;

    @Autowired
    private UserMapper userMapper;

    /**
     * 用户登录根据code获取openid
     * @param code wx.login code
     * @return openid || null
     */
    @Override
    public String login(String code) {
        JSONObject json = weChatAPI.getSessionKeyAndOpenid(code);
        if (json == null) {
            return null;
        }
        return (String) json.get("openid");
    }

    @Override
    public boolean hasUser(String openId) {
        return userMapper.getUserByOpenId(openId) != null;
    }

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }
}
