package csu.coderwu.chelper.service;


import csu.coderwu.chelper.model.entity.User;

/**
 * 定义User相关的服务
 * Created by coderWu on 17-6-10.
 */
public interface UserService {

    int addUser(User user);

    String login(String code);

    boolean hasUser(String openId);

    int updateUser(User user);

}
