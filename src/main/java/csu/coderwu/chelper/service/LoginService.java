package csu.coderwu.chelper.service;

import csu.coderwu.chelper.model.entity.Login;

/**
 * 登录态
 * Created by coderWu on 17-6-11.
 */
public interface LoginService {

    Login selectBySession(String session);

    Login selectByOpenid(String openid);

    void delete(String session);

    boolean add(String session, String openid);

    void update(Login login);

}
