package csu.coderwu.chelper.service;

import csu.coderwu.chelper.model.entity.Course;
import csu.coderwu.chelper.model.entity.Student;

import java.util.List;

/**
 * 定义Student相关Service
 * Created by coderWu on 17-6-10.
 */
public interface StudentService {

    int bind(Student student);

    boolean deleteStudent(String openId);

    boolean isBind(String openId);

    boolean login(String schoolNum, String pwd);

    Student getStudentByOpenId(String openid);

    List<Course> getGrade(Student student, String year);

    Student getStudentInfo(Student student);
}
