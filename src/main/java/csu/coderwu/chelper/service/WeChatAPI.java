package csu.coderwu.chelper.service;

import org.json.simple.JSONObject;

/**
 * WeChatAPI Interface
 * 定义需要调用微信接口实现的功能
 * Created by coderWu on 17-6-9.
 */
public interface WeChatAPI {

    JSONObject getSessionKeyAndOpenid(String code);

}
