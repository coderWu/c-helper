package csu.coderwu.chelper.config;

/**
 * 定义全局错误码
 * Created by coderWu on 17-6-10.
 */
public class ErrorCode {

    public static final String FAILED = "1001";
    public static final String MISS_PARAM = "1003";
    public static final String INVALID_PARAM = "1004";
    public static final String NOT_LOGIN = "1005";
}
