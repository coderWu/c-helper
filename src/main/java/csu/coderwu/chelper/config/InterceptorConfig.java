package csu.coderwu.chelper.config;

import csu.coderwu.chelper.interceptor.LoginInterceptor;
import csu.coderwu.chelper.interceptor.StudentInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author coderWu
 * Created in 下午2:01 17-11-11
 */
@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {

    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

    @Bean
    public StudentInterceptor studentInterceptor() {
        return new StudentInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        InterceptorRegistration ir1 = registry.addInterceptor(loginInterceptor());
        ir1.addPathPatterns("/student/**");

        InterceptorRegistration ir2 = registry.addInterceptor(studentInterceptor());
        ir2.addPathPatterns("/student/**");
        ir2.excludePathPatterns("/student/notbind");
        ir2.excludePathPatterns("/student/bind");
    }

}
