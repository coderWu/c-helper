package csu.coderwu.chelper.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author coderWu
 * @date 17-12-21 下午5:22
 */
@ConfigurationProperties(prefix = "csu.es")
public class CSUhelperProperties {

    private String xh;
    private String password;

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
