package csu.coderwu.chelper.config;

import csu.coderwu.csuhelper.cet.api.CetService;
import csu.coderwu.csuhelper.cet.api.impl.CetServiceImpl;
import csu.coderwu.csuhelper.es.api.EsService;
import csu.coderwu.csuhelper.es.api.impl.EsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author coderWu
 * @date 17-12-21 下午5:21
 */
@Configuration
@EnableConfigurationProperties(CSUhelperProperties.class)
public class CSUhelperConfig {

    @Autowired
    private CSUhelperProperties csuHelperProperties;

    @Bean
    public EsService esService() {
        return new EsServiceImpl()
                .setDefaultPwd(csuHelperProperties.getPassword())
                .setDefaultXh(csuHelperProperties.getXh());
    }

    @Bean
    public CetService cetService() {
        return new CetServiceImpl();
    }

}
