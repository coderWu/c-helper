package csu.coderwu.chelper.config;

/**
 * @author coderWu
 * @date 17-12-21 下午5:11
 */
public class Global {

    public static final String INVALID_ES_TOKEN = "-1";

    public static final int SESSION_TIME_LIMIT = 295000;

}
