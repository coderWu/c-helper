package csu.coderwu.chelper.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author coderWu
 * @date 17-12-21 下午4:37
 */
@ConfigurationProperties(prefix = "wechat.miniapp")
public class WxProperties {

    private String appid;
    private String secret;

    public String getAppid() {
        return appid;
    }

    public WxProperties setAppid(String appid) {
        this.appid = appid;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public WxProperties setSecret(String secret) {
        this.secret = secret;
        return this;
    }
}
